package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// type game struct {
// 	gameID string
// }

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	output := 0
	totalGamepower := 0

	redLimit := 12
	greenLimit := 13
	blueLimit := 14

	for scanner.Scan() {
		tempGame := map[string]int{}
		gameSplit := strings.Split(scanner.Text(), ":")
		gameIdSplit := strings.Split(gameSplit[0], " ")
		gameIDint, _ := strconv.Atoi(gameIdSplit[1])
		tempGame["gameID"] = gameIDint
		splitPulls := strings.Split(gameSplit[1], ";")
		for _, pull := range splitPulls {
			splitColors := strings.Split(pull, ",")
			for _, color := range splitColors {
				// fmt.Println(color)
				splitKeyValue := strings.Split(color, " ")
				colorValue, _ := strconv.Atoi(splitKeyValue[1])
				if colorValue > tempGame[splitKeyValue[2]] {
					tempGame[splitKeyValue[2]] = colorValue
				}

			}

		}
		if tempGame["red"] <= redLimit && tempGame["blue"] <= blueLimit && tempGame["green"] <= greenLimit {
			output = output + tempGame["gameID"]
		}
		gamePower := tempGame["red"] * tempGame["blue"] * tempGame["green"]
		totalGamepower = totalGamepower + gamePower
	}

	fmt.Println("Day 2 part 1 answer is: " + strconv.Itoa(output))
	fmt.Println("Day 2 part 2 answer is: " + strconv.Itoa(totalGamepower))
}
