package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type match struct {
	foundIndex int
	value      int
}

func main() {
	// file, err := os.Open("test-input.txt")
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	// parsedInts := []int{}
	p1Output := 0
	p2Output := 0

	for scanner.Scan() {
		fmt.Println(scanner.Text())
		fmt.Println(p2Output)
		regexSlice := []match{}
		regexSlice = checkRegex(regexSlice, scanner.Text())
		matchSlice := checkString(regexSlice, scanner.Text())
		// fmt.Println(matchSlice)
		// concatNums := (matchSlice[0] + matchSlice[len(matchSlice)-1])
		// intNum, _ := strconv.Atoi(concatNums)
		// parsedInts = append(parsedInts, intNum)
		// fmt.Println(intNum)
		p1FirstValue := findFirst(regexSlice)
		p1LastValue := findLast(regexSlice)
		p1lineOutput := p1FirstValue + p1LastValue
		fmt.Println("Part 1 Line Output: ", p1lineOutput)
		p1Output = p1Output + p1lineOutput

		p2FirstValue := findFirst(matchSlice)
		// fmt.Println("First value is ", p2FirstValue)
		p2LastValue := findLast(matchSlice)
		// fmt.Println("Last value is ", p2LastValue)
		p2lineOutput := p2FirstValue + p2LastValue
		fmt.Println("Part 2 Line Output: ", p2lineOutput)
		p2Output = p2Output + p2lineOutput
		fmt.Println(p2Output)
		fmt.Println()
	}

	fmt.Println("Day 1 part 1 answer is: ", p1Output)
	fmt.Println("Day 1 part 2 answer is: ", p2Output)
}

func checkRegex(matchSlice []match, line string) []match {
	re := regexp.MustCompile("[0-9]")
	matchRegex := (re.FindAllStringIndex(line, -1))
	for _, regexIndex := range matchRegex {
		foundMatch := match{}
		// fmt.Println(string(line[regexIndex[0]]))

		foundMatch.foundIndex = regexIndex[0]
		convertedString, err := strconv.Atoi(string(line[regexIndex[0]]))
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		foundMatch.value = convertedString

		matchSlice = append(matchSlice, foundMatch)
	}
	return matchSlice
}

func checkString(matchSlice []match, line string) []match {
	numberList := []string{"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}
	for numIndex, numString := range numberList {
		foundMatch := match{}
		if numStringMatchIndex := strings.Index(line, numString); numStringMatchIndex > -1 {
			// fmt.Println(numIndex)
			// numIndexString := strconv.Itoa(numIndex)
			foundMatch.foundIndex = numStringMatchIndex
			foundMatch.value = numIndex
			matchSlice = append(matchSlice, foundMatch)
			// fmt.Println(matchSlice)
		}
	}
	return matchSlice
}

func findFirst(matchSlice []match) int {
	firstIndex := 999999
	firstValue := 0
	for _, match := range matchSlice {
		if match.foundIndex < firstIndex {
			firstIndex = match.foundIndex
			firstValue = match.value
		}
	}
	return firstValue * 10
}

func findLast(matchSlice []match) int {
	lastIndex := -1
	lastValue := 0
	for _, match := range matchSlice {
		if match.foundIndex > lastIndex {
			lastIndex = match.foundIndex
			lastValue = match.value
		}
	}
	return lastValue
}
